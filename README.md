# TASK MANAGER

SCREENSLINK
https://drive.google.com/drive/folders/1s-dPiw-BixDa1HQlCmih-Zike-ibtJSj?usp=sharing

## DEVELOPER INFO

name: Vladimir Goloshchapov

e-mail: vgoloschapov@tsconsulting.com

## HARDWARE

CPU: i5-8250U

RAM: 16GB

SSD: 512GB

## SOFTWARE

System: WINDOWS 10

Version JDK: 1.8.

## APPLICATION BUILD

```bash
mvn clean install
```

## APPLICATION RUN
```bash
java -jar ./task-manager.jar
```
