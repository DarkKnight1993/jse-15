package ru.tsc.goloshchapov.tm.api.controller;

public interface IProjectTaskController {

    void findAllTasksByProjectId();

    void bindTaskToProjectById();

    void unbindTaskById();

}
