package ru.tsc.goloshchapov.tm.controller;

import ru.tsc.goloshchapov.tm.api.controller.IProjectTaskController;
import ru.tsc.goloshchapov.tm.api.service.IProjectService;
import ru.tsc.goloshchapov.tm.api.service.IProjectTaskService;
import ru.tsc.goloshchapov.tm.api.service.ITaskService;
import ru.tsc.goloshchapov.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.goloshchapov.tm.exception.entity.TaskNotFoundException;
import ru.tsc.goloshchapov.tm.model.Project;
import ru.tsc.goloshchapov.tm.model.Task;
import ru.tsc.goloshchapov.tm.util.TerminalUtil;

import java.util.List;

public class ProjectTaskController implements IProjectTaskController {

    private final IProjectTaskService projectTaskService;

    private final ITaskService taskService;

    private final IProjectService projectService;

    public ProjectTaskController(IProjectTaskService projectTaskService, ITaskService taskService, IProjectService projectService) {
        this.projectTaskService = projectTaskService;
        this.taskService = taskService;
        this.projectService = projectService;
    }

    @Override
    public void findAllTasksByProjectId() {
        System.out.println("[SHOW ALL TASK OF PROJECT BY PROJECT ID]");
        System.out.println("ENTER PROJECT ID");
        final String projectId = TerminalUtil.nextLine();
        final List<Task> tasks = projectTaskService.findTaskByProjectId(projectId);
        System.out.println("[TASK LIST FOR PROJECT WITH ID: " + projectId + " ]");
        int index = 1;
        for (Task task : tasks) {
            System.out.println(index + ") " + task.toString());
            index++;
        }
        System.out.println("[END LIST]");
    }

    @Override
    public void bindTaskToProjectById() {
        System.out.println("[BIND TASK TO PROJECT BY ID]");
        System.out.println("ENTER TASK ID");
        final String taskId = TerminalUtil.nextLine();
        final Task task = taskService.findById(taskId);
        if (task == null) throw new TaskNotFoundException();
        System.out.println("ENTER PROJECT ID");
        final String projectId = TerminalUtil.nextLine();
        final Project project = projectService.findById(projectId);
        if (project == null) throw new ProjectNotFoundException();
        final Task taskBinded = projectTaskService.bindTaskById(projectId, taskId);
        if (taskBinded == null) throw new TaskNotFoundException();
    }

    @Override
    public void unbindTaskById() {
        System.out.println("[UNBIND TASK FROM PROJECT BY ID]");
        System.out.println("ENTER TASK ID");
        final String taskId = TerminalUtil.nextLine();
        final Task task = taskService.findById(taskId);
        if (task == null) throw new TaskNotFoundException();
        System.out.println("ENTER PROJECT ID");
        final String projectId = TerminalUtil.nextLine();
        final Project project = projectService.findById(projectId);
        if (project == null) throw new ProjectNotFoundException();
        final Task taskUnbinded = projectTaskService.unbindTaskById(projectId, taskId);
        if (taskUnbinded == null) throw new TaskNotFoundException();
    }

}
