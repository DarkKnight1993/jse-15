package ru.tsc.goloshchapov.tm.controller;

import ru.tsc.goloshchapov.tm.api.controller.ITaskController;
import ru.tsc.goloshchapov.tm.api.service.ITaskService;
import ru.tsc.goloshchapov.tm.enumerated.Sort;
import ru.tsc.goloshchapov.tm.enumerated.Status;
import ru.tsc.goloshchapov.tm.exception.entity.TaskNotFoundException;
import ru.tsc.goloshchapov.tm.model.Task;
import ru.tsc.goloshchapov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public class TaskController implements ITaskController {

    private final ITaskService taskService;

    public TaskController(ITaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public void showTasks() {
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(Sort.values()));
        final String sort = TerminalUtil.nextLine();
        List<Task> tasks;
        if (sort == null || sort.isEmpty()) tasks = taskService.findAll();
        else {
            Sort sortType = Sort.valueOf(sort);
            System.out.println(sortType.getDisplayName());
            tasks = taskService.findAll(sortType.getComparator());
        }
        int index = 1;
        System.out.println("[LIST TASKS]");
        for (Task task : tasks) {
            System.out.println(index + ") " + task.toString());
            index++;
        }
        System.out.println("[END LIST]");
    }

    public void showTask(Task task) {
        System.out.println("[SELECTED TASK]");
        System.out.println(
                "Id: " + task.getId() +
                        "\nName: " + task.getName() +
                        "\nDescription: " + task.getDescription() +
                        "\nSatus: " + task.getStatus()
        );
        System.out.println("[END TASK]");
    }

    @Override
    public void clearTasks() {
        System.out.println("[CLEAR TASKS]");
        taskService.clear();
        System.out.println("[OK]");
    }

    @Override
    public void createTask() {
        System.out.println("[CREATE TASK]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        taskService.create(name, description);
        System.out.println("[OK]");
    }

    @Override
    public void showById() {
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        Task task = taskService.findById(id);
        if (task == null) throw new TaskNotFoundException();
        showTask(task);
    }

    @Override
    public void showByIndex() {
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        Task task = taskService.findByIndex(index);
        if (task == null) throw new TaskNotFoundException();
        showTask(task);
    }

    @Override
    public void showByName() {
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        Task task = taskService.findByName(name);
        if (task == null) throw new TaskNotFoundException();
        showTask(task);
    }

    @Override
    public void updateById() {
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        if (!taskService.existsById(id)) throw new TaskNotFoundException();
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Task taskUpdated = taskService.updateById(id, name, description);
        if (taskUpdated == null) throw new TaskNotFoundException();
    }

    @Override
    public void updateByIndex() {
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        if (!taskService.existsByIndex(index)) throw new TaskNotFoundException();
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Task taskUpdated = taskService.updateByIndex(index, name, description);
        if (taskUpdated == null) throw new TaskNotFoundException();
    }

    @Override
    public void removeById() {
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        if (!taskService.existsById(id)) throw new TaskNotFoundException();
        final Task taskRemoved = taskService.removeById(id);
        if (taskRemoved == null) throw new TaskNotFoundException();
    }

    @Override
    public void removeByIndex() {
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        if (!taskService.existsByIndex(index)) throw new TaskNotFoundException();
        final Task taskRemoved = taskService.removeByIndex(index);
        if (taskRemoved == null) throw new TaskNotFoundException();
    }

    @Override
    public void removeByName() {
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        Task task = taskService.findByName(name);
        if (task == null) throw new TaskNotFoundException();
        final Task taskRemoved = taskService.removeByName(name);
        if (taskRemoved == null) throw new TaskNotFoundException();
    }

    @Override
    public void startById() {
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.startById(id);
        if (task == null) throw new TaskNotFoundException();
    }

    @Override
    public void startByIndex() {
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.startByIndex(index);
        if (task == null) throw new TaskNotFoundException();
    }

    @Override
    public void startByName() {
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        final Task task = taskService.startByName(name);
        if (task == null) throw new TaskNotFoundException();
    }

    @Override
    public void finishById() {
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.finishById(id);
        if (task == null) throw new TaskNotFoundException();
    }

    @Override
    public void finishByIndex() {
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.finishByIndex(index);
        if (task == null) throw new TaskNotFoundException();
    }

    @Override
    public void finishByName() {
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        final Task task = taskService.finishByName(name);
        if (task == null) throw new TaskNotFoundException();
    }

    @Override
    public void changeStatusById() {
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusValue);
        final Task task = taskService.changeStatusById(id, status);
        if (task == null) throw new TaskNotFoundException();
    }

    @Override
    public void changeStatusByIndex() {
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusValue);
        final Task task = taskService.changeStatusByIndex(index, status);
        if (task == null) throw new TaskNotFoundException();
    }

    @Override
    public void changeStatusByName() {
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusValue);
        final Task task = taskService.changeStatusByName(name, status);
        if (task == null) throw new TaskNotFoundException();
    }

}
