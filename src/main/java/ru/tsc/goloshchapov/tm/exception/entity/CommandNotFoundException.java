package ru.tsc.goloshchapov.tm.exception.entity;

import ru.tsc.goloshchapov.tm.constant.TerminalConst;
import ru.tsc.goloshchapov.tm.exception.AbstractException;

public class CommandNotFoundException extends AbstractException {

    public CommandNotFoundException() {
        super("Exception! Wrong command!\nPlease, use `" + TerminalConst.HELP + "` for display list of terminal commands");
    }

}
