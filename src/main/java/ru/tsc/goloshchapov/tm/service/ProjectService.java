package ru.tsc.goloshchapov.tm.service;

import ru.tsc.goloshchapov.tm.api.repository.IProjectRepository;
import ru.tsc.goloshchapov.tm.api.service.IProjectService;
import ru.tsc.goloshchapov.tm.api.service.IProjectTaskService;
import ru.tsc.goloshchapov.tm.enumerated.Status;
import ru.tsc.goloshchapov.tm.exception.empty.*;
import ru.tsc.goloshchapov.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.goloshchapov.tm.model.Project;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ProjectService implements IProjectService {

    private final IProjectTaskService projectTaskService;

    private final IProjectRepository projectRepository;

    public ProjectService(IProjectTaskService projectTaskService, IProjectRepository projectRepository) {
        this.projectTaskService = projectTaskService;
        this.projectRepository = projectRepository;
    }

    @Override
    public void create(String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = new Project();
        project.setName(name);
        projectRepository.add(project);
    }

    @Override
    public void create(String name, String description) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        projectRepository.add(project);
    }

    @Override
    public void add(Project project) {
        if (project == null) throw new ProjectNotFoundException();
        projectRepository.add(project);
    }

    @Override
    public void remove(Project project) {
        if (project == null) throw new ProjectNotFoundException();
        projectRepository.remove(project);
    }

    @Override
    public boolean existsById(final String id) {
        if (id == null || id.isEmpty()) return false;
        return projectRepository.existsById(id);
    }

    @Override
    public boolean existsByIndex(final Integer index) {
        if (index == null || index < 0) return false;
        return projectRepository.existsByIndex(index);
    }

    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    public List<Project> findAll(final Comparator<Project> comparator) {
        if (comparator == null) return Collections.emptyList();
        return projectRepository.findAll(comparator);
    }

    @Override
    public Project findById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return projectRepository.findById(id);
    }

    @Override
    public Project findByName(final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return projectRepository.findByName(name);
    }

    @Override
    public Project findByIndex(final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        return projectRepository.findByIndex(index);
    }

    @Override
    public Project removeById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return projectTaskService.removeById(id);
    }

    @Override
    public Project removeByName(final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        Project project = projectRepository.findByName(name);
        if (project == null) throw new ProjectNotFoundException();
        return projectTaskService.removeById(project.getId());
    }

    @Override
    public Project removeByIndex(final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        Project project = projectRepository.findByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
        return projectTaskService.removeById(project.getId());
    }

    @Override
    public Project updateById(final String id, final String name, final String description) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = projectRepository.findById(id);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateByIndex(final Integer index, final String name, final String description) {
        if (index == null || index < 0) throw new EmptyIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = projectRepository.findByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project startById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return projectRepository.startById(id);
    }

    @Override
    public Project startByIndex(final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        return projectRepository.removeByIndex(index);
    }

    @Override
    public Project startByName(final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return projectRepository.startByName(name);
    }

    @Override
    public Project finishById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return projectRepository.findById(id);
    }

    @Override
    public Project finishByIndex(final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        return projectRepository.findByIndex(index);
    }

    @Override
    public Project finishByName(final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return projectRepository.finishByName(name);
    }

    @Override
    public Project changeStatusById(final String id, final Status status) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (status == null) throw new EmptyStatusException();
        return projectRepository.changeStatusById(id, status);
    }

    @Override
    public Project changeStatusByIndex(final Integer index, final Status status) {
        if (index == null || index < 0) throw new EmptyIndexException();
        if (status == null) throw new EmptyStatusException();
        return projectRepository.changeStatusByIndex(index, status);
    }

    @Override
    public Project changeStatusByName(final String name, final Status status) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (status == null) throw new EmptyStatusException();
        return projectRepository.changeStatusByName(name, status);
    }

    @Override
    public void clear() {
        projectRepository.clear();
    }

}
